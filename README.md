A small library of CSS helper classes that can be used to reduce the amount of CSS that can be added every time you need to add a new module or elements.

### Installation

You can install it using bower

```
bower install css-helpers-lib
```

Or you can download it manualy and idd it to your project.

## TODO

- [ ] Add documentation
